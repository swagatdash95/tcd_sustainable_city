describe('Side Bar', () => {
    before(() => {
        cy.visit('/')
        cy.get("[data-cy=icon]").click()
    })

    describe('Route test', () => {
        describe('Bus test', () => {
            before(() => {
                cy.get("[data-cy=Bus]").click()
            })
            it('displays the Bus url', () => {
                cy.url().should('include', 'bus')
            })
        })
        describe('Luas test', () => {
            before(() => {
                cy.get("[data-cy=Luas]").click()
            })
            it('displays the Bus url', () => {
                cy.url().should('include', 'luas')
            })
        })
        describe('Train test', () => {
            before(() => {
                cy.get("[data-cy=Rail]").click()
            })
            it('displays the Train url', () => {
                cy.url().should('include', 'train')
            })
        })
        describe('Bike test', () => {
            before(() => {
                cy.get("[data-cy=Bikes]").click()
            })
            it('displays the Bike url', () => {
                cy.url().should('include', 'bike')
            })
        })
        describe('Incident test', () => {
            before(() => {
                cy.get("[data-cy=Incident]").click()
            })
            it('displays the Incident url', () => {
                cy.url().should('include', 'incident')
            })
        })
        describe('Harzards test', () => {
            before(() => {
                cy.get("[data-cy=Harzards]").click()
            })
            it('displays the Harzards url', () => {
                cy.url().should('include', 'harzards')
            })
        })
    })
})