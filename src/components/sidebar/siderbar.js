import React from 'react'
// ui part
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DirectionsBusIcon from '@mui/icons-material/DirectionsBus';
import TrainIcon from '@mui/icons-material/Train';
import ElectricBikeIcon from '@mui/icons-material/ElectricBike';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import SubwayIcon from '@mui/icons-material/Subway';
import ReportOffIcon from '@mui/icons-material/ReportOff';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
// route
import { Link } from 'react-router-dom'
const SideBar = (props) => {
    const { setOpen, open, drawerWidth } = props
    const icons = [
        <DirectionsBusIcon />,
        <SubwayIcon />,
        <TrainIcon />,
        <ElectricBikeIcon />,
        <DeleteOutlineIcon />,
        <ReportOffIcon />,
    ]
    const routes = [
        '/bus',
        '/luas',
        '/train',
        '/bike',
        '/harzards',
        '/incident'
    ]
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };
    return (
        <>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    data-cy="icon"
                >
                    <MenuIcon />
                </IconButton>
            </Toolbar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <div className='d-flex justify-content-between align-items-center' data-cy="sideHeader">
                    Menu
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List>
                    {['Bus', 'Luas', 'Rail', 'Bikes', 'Harzards', 'Incident Reports'].map((text, index) => (
                        <Link to={routes[index]} style={{ textDecoration: 'none', color: 'black' }} data-cy={text.split(' ')[0]}>
                            <ListItem button key={text} className='mt-3'>
                                <ListItemIcon>
                                    {icons[index]}
                                </ListItemIcon>
                                <ListItemText primary={text} />
                            </ListItem>
                        </Link>
                    ))}
                </List>
            </Drawer>
        </>
    )
}

export default SideBar