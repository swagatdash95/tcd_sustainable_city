import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import SideBar from './components/sidebar/siderbar';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import BikePage from './pages/BikePage'
import BusPage from './pages/BusPage'
import HarzardsPage from './pages/HarzardsPage';
import LuasPage from './pages/LuasPage';
import TrainPage from './pages/TrainPage';
import IncidentPage from './pages/IncidentPage';
import HomePage from './pages/HomePage';

const drawerWidth = 240
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);


function App() {
  const [open, setOpen] = React.useState(false);
  return (
    <BrowserRouter>
      <Box sx={{ display: 'flex', alignItems: 'start' }}>
        <SideBar setOpen={setOpen} open={open} drawerWidth={drawerWidth} />
        <Main open={open}>
          <Routes>
            <Route path="/train" element={<TrainPage />} />
            <Route path="/luas" element={<LuasPage />} />
            <Route path="/bike" element={<BikePage />} />
            <Route path="/bus" element={<BusPage />} />
            <Route path="/harzards" element={<HarzardsPage />} />
            <Route path="/incident" element={<IncidentPage />} />
            <Route path="/" element={<HomePage />} />
          </Routes>
        </Main>
      </Box>
    </BrowserRouter>
  );
}

export default App;
